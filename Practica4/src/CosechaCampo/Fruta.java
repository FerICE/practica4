package CosechaCampo;

import CosechaCampo.Periodo;

public class Fruta {
	public static void main(String[]args) {
	
	}
	private String nombre = "";
	private float extencion = 0f;
	private float costoPromedio = 0f;
	private float precioVentaProm = 0f;
	private Periodo  periodo;
	
	public void addperiodo(Periodo periodo) {
		Periodo.add(periodo);
	}

	Periodo P10 = new Periodo ("3 meses",10);
	
	
	

	public Fruta() {
		super ();
	}
	public Fruta(String nombre, float extencion, float costoPromedio, float precioVentaProm, Periodo periodo) {
		super();
		this.nombre = nombre;
		this.extencion = extencion;
		this.costoPromedio = costoPromedio;
		this.precioVentaProm = precioVentaProm;
		this.periodo = periodo;
	}
		

	
	
	
	
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public float getExtencion() {
		return extencion;
	}
	public void setExtencion(float extencion) {
		this.extencion = extencion;
	}
	public float getCostoPromedio() {
		return costoPromedio;
	}
	public void setCostoPromedio(float costoPromedio) {
		this.costoPromedio = costoPromedio;
	}
	public float getPrecioVentaProm() {
		return precioVentaProm;
	}
	public void setPrecioVentaProm(float precioVentaProm) {
		this.precioVentaProm = precioVentaProm;
	}
	public Periodo getPeriodo() {
		return periodo;
	}
	public void agregarPeriodo(Periodo periodo){
		return;
	}
		
public boolean eliminarPeriodo(Periodo periodo) {
	return false;
}
	public String toString() {
		return "Fruta [nombre=" + nombre + ", extencion=" + extencion + ", costoPromedio=" + costoPromedio
				+ ", precioVentaProm=" + precioVentaProm + ", periodo=" + periodo + "]";
	}
	

}
